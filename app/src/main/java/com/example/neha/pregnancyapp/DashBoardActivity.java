package com.example.neha.pregnancyapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    LinearLayout settingDrawerLL;
    private static boolean doubleBackToExitPressedOnce = false;


    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "PregnancyAppPrefs";
//    Map<Integer, String> monthImgMap = new HashMap<Integer, String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);


        setToolbarDrawer();
//        changeStatusBarColor();


    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
        Button dashboardTV = (Button) findViewById(R.id.dashboardTV);
        Button monthalyProgressTV = (Button) findViewById(R.id.monthalyProgressTV);
        Button aboutUsTV = (Button) findViewById(R.id.aboutUsTV);
        Button helpTV = (Button) findViewById(R.id.helpTV);
        Button logoutTV = (Button) findViewById(R.id.logoutBTN);


        dashboardTV.setOnClickListener(this);
        monthalyProgressTV.setOnClickListener(this);
        aboutUsTV.setOnClickListener(this);
        helpTV.setOnClickListener(this);
        logoutTV.setOnClickListener(this);
//        settingDrawerLL.setOnClickListener(this);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBarColor));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.bottomBTN:
//                getallMonthImages(12);
//                Bundle b = new Bundle();
//                b.putSerializable("response_images",imagesArrayList);
                intent = new Intent(this, MonthActivity.class);
//                intent.putExtras(b);
                startActivity(intent);
                break;
            case R.id.donndont:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 1);
                intent.putExtra("Title", "Do's & Dont's");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked DO&DONT", Toast.LENGTH_SHORT).show();
                break;
            case R.id.music:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 2);
                intent.putExtra("Title", "Music");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked music", Toast.LENGTH_SHORT).show();
                break;
            case R.id.second_opinion:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 3);
                intent.putExtra("Title", "Second Opinion");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked second_opinion", Toast.LENGTH_SHORT).show();
                break;
            case R.id.beautiful_memories:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 4);
                intent.putExtra("Title", "Beautiful Memories");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked beautiful_memories", Toast.LENGTH_SHORT).show();
                break;
            case R.id.diet_planner:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 5);
                intent.putExtra("Title", "Diet Planner");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked diet_planner", Toast.LENGTH_SHORT).show();
                break;
            case R.id.exercise:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 6);
                intent.putExtra("Title", "Exercise");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked exercise", Toast.LENGTH_SHORT).show();
                break;
            case R.id.yoga:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 7);
                intent.putExtra("Title", "Yoga");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked yoga", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tips:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 8);
                intent.putExtra("Title", "Tips To Beat Stress");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked tips", Toast.LENGTH_SHORT).show();
                break;
            case R.id.mat_cloths:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 9);
                intent.putExtra("Title", "Maternity Cloths");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked mat_cloths", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imp_vaccines:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 10);
                intent.putExtra("Title", "Important Vaccines");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked imp_vaccines", Toast.LENGTH_SHORT).show();
                break;
            case R.id.baby_shower:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 11);
                intent.putExtra("Title", "Baby Shower");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked baby_shower", Toast.LENGTH_SHORT).show();
                break;
            case R.id.wellness_center:
                intent = new Intent(this, DashboardOptionActivity.class);
                intent.putExtra("MarkerId", 12);
                intent.putExtra("Title", "Wellness Center around");
                startActivity(intent);
//                Toast.makeText(this, "You Clicked wellness_center", Toast.LENGTH_SHORT).show();
                break;

            case R.id.dashboardTV:
                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "dashboard");
                break;
            case R.id.monthalyProgressTV:
                SplashScreenActivity.onDrawerOptionClick(DashBoardActivity.this, v.getId(), "dashboard");
//                SplashScreenActivity.onDrawerOptionClick(DashBoardActivity.this, v.getId(), "dashboard");
                break;
            case R.id.aboutUsTV:
                SplashScreenActivity.onDrawerOptionClick(DashBoardActivity.this, v.getId(), "dashboard");
                break;
            case R.id.helpTV:
                SplashScreenActivity.onDrawerOptionClick(DashBoardActivity.this, v.getId(), "dashboard");
                break;
            case R.id.logoutBTN:
                SplashScreenActivity.logout_fun(DashBoardActivity.this);
                break;

        }

    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();

            this.finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit from Pregnancy App", Toast.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                SplashScreenActivity.logout_fun(DashBoardActivity.this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
