package com.example.neha.pregnancyapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class SplashScreenActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "PregnancyAppPrefs";
    int loggedin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        changeStatusBarColor();

        handler();
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBarColorSplash));
//            window.setStatusBarTextColor(getResources().getColor(R.color.orange));
        }
    }

    public void handler() {
        new Handler().postDelayed(new Runnable() {


            // Using handler with postDelayed called runnable run method
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

                loggedin = sharedPreferences.getInt("login", 0);

                if (loggedin == 1) {
                    Intent intent = new Intent(SplashScreenActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }

            }
        }, 5 * 1000); // wait for 5 seconds
    }

    public static void networkCheckDialog(Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Network Connection");
        builder.setMessage("Please Check your internet connection.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.show();
    }


    public static void onDrawerOptionClick(Activity activity, int id, String comeFrom) {
        Intent intent;
        switch (id) {
            case R.id.dashboardTV:
                if (!comeFrom.equalsIgnoreCase("dashboard")) {
                    intent = new Intent(activity, DashBoardActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                } else
                    Toast.makeText(activity, "You are already on Dashboard", Toast.LENGTH_SHORT).show();
                break;
            case R.id.monthalyProgressTV:
                if (!comeFrom.equalsIgnoreCase("main")) {
                    intent = new Intent(activity, MonthActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                } else
                    Toast.makeText(activity, "You are already on Monthly Progress", Toast.LENGTH_SHORT).show();
                break;
            case R.id.aboutUsTV:
                intent = new Intent(activity,AboutUsActivity.class);
                activity.startActivity(intent);
                activity.finish();
//                Toast.makeText(activity, "You Clicked about us.....under development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.helpTV:
                intent = new Intent(activity,HelpActivity.class);
                activity.startActivity(intent);
                activity.finish();
//                Toast.makeText(activity, "You Clicked help.....under development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.loginBTN:
                logout_fun(activity);
                break;

        }


    }


    public static void logout_fun(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context,
                        LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Toast.makeText(context,
                        "You are logout successfully", Toast.LENGTH_LONG).show();
                SharedPreferences settings = context.getApplicationContext()
                        .getSharedPreferences(MyPREFERENCES,
                                Context.MODE_PRIVATE);
                settings.edit().clear().commit();
                context.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();


    }


}
