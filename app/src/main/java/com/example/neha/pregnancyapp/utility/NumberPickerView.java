package com.example.neha.pregnancyapp.utility;

/**
 * Created by neha on 9/15/2016.
 */

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.neha.pregnancyapp.R;


public class NumberPickerView extends LinearLayout {

    private Button decrementBTN, incrementBTN;
    private TextView valueTV;

    private boolean isEnabled = true;

    private final int MINIMUM = 1;
    private final int MAXIMUM = 45;

    public NumberPickerView(Context mContext, AttributeSet attributeSet) {
        super(mContext, attributeSet);

        this.setBackgroundColor(Color.GRAY);
        this.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        LayoutParams elementParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
                1);
        elementParams.setMargins(1, 1, 1, 1);
        LayoutParams editParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT,
                1);
        editParams.setMargins(1, 1, 1, 1);

        initDecrementButton(mContext);
        initValueEditText(mContext);
        initIncrementButton(mContext);

        addView(decrementBTN, elementParams);
        addView(valueTV, editParams);
        addView(incrementBTN, elementParams);
        isEnabled = true;
    }

    private void initIncrementButton(final Context mContext) {
        incrementBTN = new Button(mContext);
        incrementBTN.setText("+");
        incrementBTN.setTextColor(getResources().getColor(R.color.white));
        incrementBTN.setBackground(getResources().getDrawable(R.drawable.pink_button));
        incrementBTN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    incrementValue();
                else
                    Toast.makeText(mContext, R.string.npv_edit_from_cart, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initValueEditText(Context mContext) {
        valueTV = new TextView(mContext);
        valueTV.setTextColor(getResources().getColor(R.color.textColor));
        valueTV.setBackgroundColor(getResources().getColor(R.color.white));
        valueTV.setGravity(Gravity.CENTER);
        valueTV.setText(String.valueOf(MINIMUM));
    }

    private void initDecrementButton(final Context mContext) {
        decrementBTN = new Button(mContext);
        decrementBTN.setText("-");
        decrementBTN.setTextColor(getResources().getColor(R.color.white));
        decrementBTN.setBackground(getResources().getDrawable(R.drawable.pink_button));

        decrementBTN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    decrementValue();
                else
                    Toast.makeText(mContext, R.string.npv_edit_from_cart, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void incrementValue() {
        int value = Integer.parseInt(valueTV.getText().toString());
        if (value < MAXIMUM) {
            value += 1;
            valueTV.setText(String.valueOf(value));
        }
    }

    private void decrementValue() {
        int value = Integer.parseInt(valueTV.getText().toString());
        if (value > MINIMUM) {
            value -= 1;
            valueTV.setText(String.valueOf(value));
        }
    }

    public void setValue(int value) {
        if (value > MAXIMUM) {
            valueTV.setText(String.valueOf(MAXIMUM));
        } else if (value < MINIMUM) {
            valueTV.setText(String.valueOf(MINIMUM));
        } else {
            valueTV.setText(String.valueOf(value));
        }
    }

    public int getValue() {
        return Integer.parseInt(valueTV.getText().toString());
    }

    public void setEnabled(boolean state) {
        isEnabled = state;
    }
}
