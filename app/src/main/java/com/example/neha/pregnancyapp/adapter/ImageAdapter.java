package com.example.neha.pregnancyapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neha.pregnancyapp.MonthProgressActivity;
import com.example.neha.pregnancyapp.R;
import com.example.neha.pregnancyapp.utility.Constants;
import com.example.neha.pregnancyapp.utility.ImagePicker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neha on 9/20/2016.
 */
public class ImageAdapter extends BaseAdapter {
    Activity mContext;
    List<String> imagesArrayList;
    String[] monthsNameArr;

    // Constructor
    public ImageAdapter(Activity c, ArrayList<String> imagesArrayList, String[] monthsNameArr) {

        this.mContext = c;
        this.imagesArrayList = imagesArrayList;
        this.monthsNameArr = monthsNameArr;
    }

    public int getCount() {
        return imagesArrayList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        View grid;
        final ImageView imageView;
        final TextView monthNameTV;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            grid = inflater.inflate(R.layout.grid_item_layout, parent, false);
        } else {
            grid = (View) convertView;
        }
        imageView = (ImageView) grid.findViewById(R.id.imageViewGrid);
        monthNameTV = (TextView) grid.findViewById(R.id.monthNameTV);
        monthNameTV.setText(monthsNameArr[position].toString());
        if (imagesArrayList.get(position).toString().endsWith(".png")) {
            Picasso.with(mContext).load(imagesArrayList.get(position).toString()).into(imageView);
        } else {
            imageView.setImageResource(Integer.parseInt(imagesArrayList.get(position)));
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.MONTHID = position;
                ImagePicker.selectImage(mContext, imageView);
            }
        });

        monthNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MonthProgressActivity.class);
                intent.putExtra("MarkerId", position);
                intent.putExtra("ComeFrom", "MainActivity");
                mContext.startActivity(intent);
                mContext.finish();
            }
        });
        return grid;
    }
}
