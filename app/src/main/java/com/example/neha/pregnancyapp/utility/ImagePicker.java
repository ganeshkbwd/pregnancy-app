package com.example.neha.pregnancyapp.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.R;
import com.example.neha.pregnancyapp.adapter.ImageAdapter;
import com.example.neha.pregnancyapp.modelClass.GetAllImages;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 11/28/2016.
 */
public class ImagePicker {
    private static final int SELECT_FILE = 11;
    private static final int REQUEST_CAMERA = 12;
    static String encodedImage;

    public static void selectImage(final Activity activity, ImageView imageView) {
        Constants.imageView = imageView;
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    activity.startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");

                    activity.startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public static void onSelectFromGalleryResult(Intent data, Activity activity, int user_id, ArrayList<String> month_string, String[] monthsNameArr, ArrayList<GetAllImages> imagesArrayList, GridView gridview) {
        Uri selectedImageUri = data.getData();

        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity.managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
//
//
        Constants.imageView.setImageBitmap(scaledBitmap);
        Constants.photo = scaledBitmap;
//        imageUpload(activity, user_id, month_string, monthsNameArr, imagesArrayList, gridview);
        imageUpload(activity,user_id);
    }


    public static void onCaptureImageResult(Intent data, Activity activity, int user_id, ArrayList<String> month_string, String[] monthsNameArr, ArrayList<GetAllImages> imagesArrayList, GridView gridview) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
//
//
        Constants.imageView.setImageBitmap(scaledBitmap);
        Constants.photo = scaledBitmap;
//        encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
//
//
//        ivProfile.setImageBitmap(scaledBitmap);
//        photo = scaledBitmap;
//        imageUpload(activity, user_id, month_string, monthsNameArr, imagesArrayList, gridview);
        imageUpload(activity, user_id);
    }

    public static void imageUpload(final Activity activity, final int user_id) {

        //, final int user_id, final ArrayList<String> month_string, final String[] monthsNameArr, final ArrayList<GetAllImages> imagesArrayList, final GridView gridview
//        getStringImage(bitmap);
        Constants.progressDialog = new ProgressDialog(activity);
        Constants.progressDialog.setMessage("Uploading ....");
        Constants.progressDialog.show();

        final int month = Constants.MONTHID + 1;
        String uploadurl = "http://designer321.com/rajohn/pregnacycapp/api/save_monthly_progress.php";

        RequestQueue queue = Volley.newRequestQueue(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, uploadurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");

                            if (message_code == 1) {

                                Constants.imageView.setImageBitmap(Constants.photo);
                                Constants.progressDialog.dismiss();
                                Intent intent = activity.getIntent();
                                activity.finish();
                                activity.startActivity(intent);
//                                getallMonthImages(user_id, activity, month_string, monthsNameArr, imagesArrayList, gridview);
                            } else {
                                String message = jobj.getString("message");
                                Constants.progressDialog.dismiss();
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();

                Toast.makeText(activity, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = encodedImage;
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(user_id));
                params.put("month", month + "");
                params.put("image_path", image);

                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }


    public static void getallMonthImages(int pregnancy_user_id, final Activity activity, final ArrayList<String> month_string, final String[] monthsNameArr, final ArrayList<GetAllImages> imagesArrayList, final GridView gridview) {
        Constants.progressDialog = new ProgressDialog(activity);
        Constants.progressDialog.setMessage("Loading ....");
        Constants.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/get_all_monthly_images.php?user_id=" + pregnancy_user_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
                            if (message_code == 1) {
                                imagesArrayList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    GetAllImages getAllImages = new GetAllImages();
                                    getAllImages.setImagePath(jsonArray.getJSONObject(i).getString("image_path"));
                                    getAllImages.setMonth(jsonArray.getJSONObject(i).getInt("month"));

                                    month_string.add(jsonArray.getJSONObject(i).getString("image_path"));
                                    imagesArrayList.add(getAllImages);
                                }

                                if (imagesArrayList.size() == 0) {
                                    month_string.add(String.valueOf(R.drawable.girl_default_pic));

                                } else if (imagesArrayList.size() == 1) {
                                    month_string.add(String.valueOf(R.drawable.get_ready_text));

                                } else if (imagesArrayList.size() == 2) {
                                    month_string.add(String.valueOf(R.drawable.missed_period_text));
                                } else if (imagesArrayList.size() == 3) {
                                    month_string.add(String.valueOf(R.drawable.month3));
                                } else if (imagesArrayList.size() == 4) {
                                    month_string.add(String.valueOf(R.drawable.month4));
                                } else if (imagesArrayList.size() == 5) {
                                    month_string.add(String.valueOf(R.drawable.month5));
                                } else if (imagesArrayList.size() == 6) {
                                    month_string.add(String.valueOf(R.drawable.month6));
                                } else if (imagesArrayList.size() == 7) {
                                    month_string.add(String.valueOf(R.drawable.month7));
                                } else if (imagesArrayList.size() == 8) {
                                    month_string.add(String.valueOf(R.drawable.month8));
                                } else if (imagesArrayList.size() == 9) {
                                    month_string.add(String.valueOf(R.drawable.month9));
                                } else if (imagesArrayList.size() == 10) {
                                    month_string.add(String.valueOf(R.drawable.mom_baby_default_pic));
                                }
//                                for (int i = 0; i < month_string.size(); i++) {
//                                    if (imagesArrayList.get(i).getMonth() < 12) {
//                                        if (imagesArrayList.get(i).getMonth() != 1) {
//                                            month_string.set((imagesArrayList.get(i).getMonth()) - 1, imagesArrayList.get(i).getImagePath());
//                                        }
//
//                                        Log.e("imagesArrayList1", month_string.get(i).toString());
//                                    }
//                                }
                                gridview.setAdapter(new ImageAdapter(activity, month_string, monthsNameArr));
                                Log.e("imagesArrayList", imagesArrayList.toString());
                                Constants.progressDialog.dismiss();

                            } else {
                                month_string.add(String.valueOf(R.drawable.girl_default_pic));
                                gridview.setAdapter(new ImageAdapter(activity, month_string, monthsNameArr));
//                                String message = jobj.getString("message");
//                                Toast.makeText(MonthActivity.this, message, Toast.LENGTH_SHORT).show();
                                Constants.progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();
                Toast.makeText(activity, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();

            }
        });


        queue.add(stringRequest);


    }

}
