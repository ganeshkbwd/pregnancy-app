package com.example.neha.pregnancyapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.utility.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email_idET, passET;
    Button loginBTN, signupBTN;
    TextView fpBTN;
    ProgressDialog progressDialog;
    String message;
    int message_code, pregnancy_user_id;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "PregnancyAppPrefs";
    int loggedin;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email_idET = (EditText) findViewById(R.id.email_idET);
        passET = (EditText) findViewById(R.id.passET);
        loginBTN = (Button) findViewById(R.id.loginBTN);
        fpBTN = (TextView) findViewById(R.id.fpBTN);
        signupBTN = (Button) findViewById(R.id.signupBTN);
//        imageView = (ImageView) findViewById(R.id.imageView);
//        Uri uri = Uri.parse("android.resource://com.example.neha.pregnancyapp/drawable/image_name");

//        Picasso.with(this).load(uri).into(imageView);

        loginBTN.setOnClickListener(this);
        fpBTN.setOnClickListener(this);
        signupBTN.setOnClickListener(this);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);




    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBTN:
                if (email_idET.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(email_idET.getText().toString().trim()).matches()) {
                    if (passET.getText().toString().trim().length() > 0) {
                        loginCheck(email_idET.getText().toString().trim(), passET.getText().toString().trim());
                    } else {
                        Toast.makeText(LoginActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fpBTN:
                forgotPassword();

                break;
            case R.id.signupBTN:
                Intent intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);

                break;
        }
    }

    public void forgotPassword(){
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.forgot_password
                , null));
        final EditText fppassTV = (EditText) settingsDialog.findViewById(R.id.fppassURL);
        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fppassTV.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(fppassTV.getText().toString().trim()).matches()) {
                    forgotPassword(fppassTV.getText().toString(), settingsDialog);
                } else {
                    Toast.makeText(LoginActivity.this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                }
            }
        });

        settingsDialog.show();
    }

    public void loginCheck(String email, String pass) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging ....");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/login.php?email_address=" + email + "&password=" + pass,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            message_code = jobj.getInt("message_code");
                            message = jobj.getString("message");
                            if (message_code == 1) {
                                pregnancy_user_id = jobj.getInt("pregnancy_user_id");
                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("user_id", pregnancy_user_id);

                                editor.commit();


                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, "You have successfully logged in", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(LoginActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });


        queue.add(stringRequest);


    }

    public void forgotPassword(String email, final Dialog settingsDialog) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending ....");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/forgotpassword.php?email_address=" + email,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            message_code = jobj.getInt("message_code");
                            message = jobj.getString("message");

                            if (message_code == 1) {
                                progressDialog.dismiss();
                                settingsDialog.dismiss();
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                            } else {
                                progressDialog.dismiss();
                                settingsDialog.dismiss();
                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);


    }

}
