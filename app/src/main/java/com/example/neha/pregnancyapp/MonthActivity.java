package com.example.neha.pregnancyapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.adapter.ImageAdapter;
import com.example.neha.pregnancyapp.modelClass.GetAllImages;
import com.example.neha.pregnancyapp.utility.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MonthActivity extends AppCompatActivity {
    ArrayList<GetAllImages> allImagesArrayList;
    ProgressDialog progressDialog;
    ArrayList<GetAllImages> imagesArrayList;
    ArrayList<String> month_string = new ArrayList<String>();
    GridView gridview;
    SharedPreferences sharedPreferences;
    int loggedin, user_id;
    public static final String MyPREFERENCES = "PregnancyAppPrefs";
    private static final int SELECT_FILE = 11;
    private static final int REQUEST_CAMERA = 12;
    String[] monthsNameArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        loggedin = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        gridview = (GridView) findViewById(R.id.gridView);
        monthsNameArr = getResources().getStringArray(R.array.months_name);

        imagesArrayList = new ArrayList<GetAllImages>();
//        addImages();
        getallMonthImages(user_id);
//        ImagePicker.getallMonthImages(user_id, this, month_string, monthsNameArr, imagesArrayList, gridview);

//        gridview.setOnItemClickListener(this);

    }

//    public void addImages() {
//
//        month_string.add(String.valueOf(R.drawable.girl_default_pic));
//        month_string.add(String.valueOf(R.drawable.get_ready_text));
//        month_string.add(String.valueOf(R.drawable.missed_period_text));
//        month_string.add(String.valueOf(R.drawable.month3));
//        month_string.add(String.valueOf(R.drawable.month4));
//        month_string.add(String.valueOf(R.drawable.month5));
//        month_string.add(String.valueOf(R.drawable.month6));
//        month_string.add(String.valueOf(R.drawable.month7));
//        month_string.add(String.valueOf(R.drawable.month8));
//        month_string.add(String.valueOf(R.drawable.month9));
//        month_string.add(String.valueOf(R.drawable.mom_baby_default_pic));
//
//    }

    public void getallMonthImages(int pregnancy_user_id) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/get_all_monthly_images.php?user_id=" + pregnancy_user_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
                            if (message_code == 1) {

                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    GetAllImages getAllImages = new GetAllImages();
                                    getAllImages.setImagePath(jsonArray.getJSONObject(i).getString("image_path"));
                                    getAllImages.setMonth(jsonArray.getJSONObject(i).getInt("month"));

                                    month_string.add(jsonArray.getJSONObject(i).getString("image_path"));
                                    imagesArrayList.add(getAllImages);
                                }

                                if (imagesArrayList.size() == 0) {
                                    month_string.add(String.valueOf(R.drawable.girl_default_pic));

                                } else if (imagesArrayList.size() == 1) {
                                    month_string.add(String.valueOf(R.drawable.get_ready_text));

                                } else if (imagesArrayList.size() == 2) {
                                    month_string.add(String.valueOf(R.drawable.missed_period_text));
                                } else if (imagesArrayList.size() == 3) {
                                    month_string.add(String.valueOf(R.drawable.month3));
                                } else if (imagesArrayList.size() == 4) {
                                    month_string.add(String.valueOf(R.drawable.month4));
                                } else if (imagesArrayList.size() == 5) {
                                    month_string.add(String.valueOf(R.drawable.month5));
                                } else if (imagesArrayList.size() == 6) {
                                    month_string.add(String.valueOf(R.drawable.month6));
                                } else if (imagesArrayList.size() == 7) {
                                    month_string.add(String.valueOf(R.drawable.month7));
                                } else if (imagesArrayList.size() == 8) {
                                    month_string.add(String.valueOf(R.drawable.month8));
                                } else if (imagesArrayList.size() == 9) {
                                    month_string.add(String.valueOf(R.drawable.month9));
                                } else if (imagesArrayList.size() == 10) {
                                    month_string.add(String.valueOf(R.drawable.mom_baby_default_pic));
                                }
//                                for (int i = 0; i < month_string.size(); i++) {
//                                    if (imagesArrayList.get(i).getMonth() < 12) {
//                                        if (imagesArrayList.get(i).getMonth() != 1) {
//                                            month_string.set((imagesArrayList.get(i).getMonth()) - 1, imagesArrayList.get(i).getImagePath());
//                                        }
//
//                                        Log.e("imagesArrayList1", month_string.get(i).toString());
//                                    }
//                                }
                                gridview.setAdapter(new ImageAdapter(MonthActivity.this, month_string, monthsNameArr));
                                Log.e("imagesArrayList", imagesArrayList.toString());
                                progressDialog.dismiss();

                            } else {
                                month_string.add(String.valueOf(R.drawable.girl_default_pic));
                                gridview.setAdapter(new ImageAdapter(MonthActivity.this, month_string, monthsNameArr));
//                                String message = jobj.getString("message");
//                                Toast.makeText(MonthActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MonthActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();

            }
        });


        queue.add(stringRequest);


    }

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////        if (position != 0) {
//
//        Intent intent = new Intent(this, MonthProgressActivity.class);
//        intent.putExtra("MarkerId", position);
//        intent.putExtra("ComeFrom", "MainActivity");
//        startActivity(intent);
//        finish();
////        } else {
////
////
////        }
//    }

    @Override
    public void onBackPressed() {

//        super.onBackPressed();
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                Intent selectedImageResultIntent = data;

                ImagePicker.onSelectFromGalleryResult(data, MonthActivity.this,user_id,month_string,monthsNameArr,imagesArrayList,gridview);
//                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                //checkPermissions();
//                CaptureMethods.onCaptureImageResult(data, MonthProgressActivity.this, ivProfile);
                ImagePicker.onCaptureImageResult(data, MonthActivity.this,user_id,month_string,monthsNameArr,imagesArrayList,gridview);
            }

        }
    }


}
