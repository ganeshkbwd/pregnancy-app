package com.example.neha.pregnancyapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity {
    EditText fullnameET, email_idET, contactET, passET;
    Button signupBTN;
    ProgressDialog progressDialog;
    String message;
    int message_code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        fullnameET = (EditText) findViewById(R.id.fullnameET);
        email_idET = (EditText) findViewById(R.id.email_idET);
        contactET = (EditText) findViewById(R.id.contactET);
        passET = (EditText) findViewById(R.id.passET);
        signupBTN = (Button) findViewById(R.id.signupBTN);

        signupBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fullnameET.getText().toString().trim().length() > 0) {
                    if (email_idET.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(email_idET.getText().toString().trim()).matches()) {
                        if (contactET.getText().toString().trim().length() > 0) {
                            if (passET.getText().toString().trim().length() > 0) {
                                signup(fullnameET.getText().toString().trim(), email_idET.getText().toString().trim(), contactET.getText().toString().trim(), passET.getText().toString().trim());
                            } else {
                                Toast.makeText(SignUpActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(SignUpActivity.this, "Please enter contact no.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, "Please enter your valid email_id", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, "Please enter your full name", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void signup(String fullname, String email_id, String contact, String password) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait ....");
        progressDialog.show();

        String signupURL = "http://designer321.com/rajohn/pregnacycapp/api/register.php?full_name=" + fullname + "&email_address=" + email_id + "&password=" + password + "&phone_number=" + contact;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, signupURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            message_code = jobj.getInt("message_code");
                            message = jobj.getString("message");

                            if (message_code == 1) {
                                progressDialog.dismiss();
                                Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(SignUpActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
