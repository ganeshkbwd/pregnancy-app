package com.example.neha.pregnancyapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.adapter.ViewPagerAdapter;
import com.example.neha.pregnancyapp.fragments.OneFragment;
import com.example.neha.pregnancyapp.fragments.TwoFragment;
import com.example.neha.pregnancyapp.modelClass.DashboardData;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DashboardOptionActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Bundle args;
    DashboardData dashboardData = new DashboardData();
    private GoogleApiClient client;
    int id;
    String title;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_option);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        setToolbarDrawer();
        getDataFromServer(id);


    }


    public void getDataFromServer(int i) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/get_selected_menu_details.php?menu_id=" + i,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");

                            if (message_code == 1) {
                                JSONArray responseArr = jobj.getJSONArray("response");


                                for (int i = 0; i < responseArr.length(); i++) {

                                    dashboardData.setTitle(responseArr.getJSONObject(i).getString("title"));
                                    if (!responseArr.getJSONObject(i).getString("description").equalsIgnoreCase(""))
                                        dashboardData.setDescription(responseArr.getJSONObject(i).getString("description"));
                                    else
                                        dashboardData.setDescription("-");
                                    if (!responseArr.getJSONObject(i).getString("menu_name").equalsIgnoreCase(""))
                                        dashboardData.setMenu_name(responseArr.getJSONObject(i).getString("menu_name"));
                                    else
                                        dashboardData.setMenu_name("-");
                                    if (!responseArr.getJSONObject(i).getString("preg_media1").equalsIgnoreCase("null"))
                                        dashboardData.setPreg_media1(responseArr.getJSONObject(i).getString("preg_media1"));

                                    else
                                        dashboardData.setPreg_media1("-");
                                    if (!responseArr.getJSONObject(i).getString("preg_media2").equalsIgnoreCase(""))
                                        dashboardData.setPreg_media2(responseArr.getJSONObject(i).getString("preg_media2"));
                                    else
                                        dashboardData.setPreg_media2("-");
                                    if (!responseArr.getJSONObject(i).getString("preg_media3").equalsIgnoreCase(""))
                                        dashboardData.setPreg_media3(responseArr.getJSONObject(i).getString("preg_media3"));
                                    else
                                        dashboardData.setPreg_media3("-");
                                    if (!responseArr.getJSONObject(i).getString("preg_media4").equalsIgnoreCase(""))
                                        dashboardData.setPreg_media4(responseArr.getJSONObject(i).getString("preg_media4"));
                                    else
                                        dashboardData.setPreg_media4("-");
                                    if (!responseArr.getJSONObject(i).getString("time").equalsIgnoreCase(""))
                                        dashboardData.setTime(responseArr.getJSONObject(i).getString("time"));
                                    else
                                        dashboardData.setTime("-");


                                    if (!responseArr.getJSONObject(i).getString("price").equalsIgnoreCase(""))
                                        dashboardData.setPrice(responseArr.getJSONObject(i).getString("price"));
                                    else
                                        dashboardData.setPrice("-");
                                    if (!responseArr.getJSONObject(i).getString("name").equalsIgnoreCase(""))
                                        dashboardData.setName(responseArr.getJSONObject(i).getString("name"));
                                    else
                                        dashboardData.setName("-");
                                    if (!responseArr.getJSONObject(i).getString("hospital_name").equalsIgnoreCase(""))
                                        dashboardData.setHospital_name(responseArr.getJSONObject(i).getString("hospital_name"));
                                    else
                                        dashboardData.setHospital_name("-");
                                    if (!responseArr.getJSONObject(i).getString("hospital_address").equalsIgnoreCase(""))
                                        dashboardData.setHospital_address(responseArr.getJSONObject(i).getString("hospital_address"));
                                    else
                                        dashboardData.setHospital_address("-");
//

                                }
                                Log.e("GIVEN DATA", dashboardData.toString());
                                setupViewPager();
                                progressDialog.dismiss();
                            } else {
                                String message = jobj.getString("message");
                                progressDialog.dismiss();
                                Toast.makeText(DashboardOptionActivity.this, message, Toast.LENGTH_SHORT).show();
                            }

//


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(DashboardOptionActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);


    }


    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleToolbar);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("MarkerId");
        title = bundle.getString("Title");
        titleTV.setText(title);


    }

    private void setupViewPager() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        OneFragment oneFragment = new OneFragment();
        TwoFragment twoFragment = new TwoFragment();

        args = new Bundle();
        args.putSerializable("RESPONSE_DATA", dashboardData);
        oneFragment.setArguments(args);

        twoFragment.setArguments(args);
        adapter.addFragment(oneFragment, "Description");
        adapter.addFragment(twoFragment, "Media");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DashboardOption Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.neha.pregnancyapp/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DashboardOption Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.neha.pregnancyapp/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DashboardOptionActivity.this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }
}
