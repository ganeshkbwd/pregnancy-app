package com.example.neha.pregnancyapp;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

public class VideoPlayActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    VideoView videoview;
    ImageView imageView;
    String url;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        Bundle bundle = getIntent().getExtras();
        url = bundle.getString("URL");
        Uri uri = Uri.parse(url);

        videoview = (VideoView) findViewById(R.id.VideoView);
        imageView = (ImageView) findViewById(R.id.imageView);

        // Execute StreamVideo AsyncTask
        if (url.endsWith(".mp3") || url.endsWith(".mp4")) {

            // Create a progressbar
            pDialog = new ProgressDialog(VideoPlayActivity.this);
            // Set progressbar title
            if (url.endsWith(".mp4")||url.endsWith(".mov")) {

                pDialog.setTitle("Please wait Video Streaming");
                asyncVideo(uri);
            } else {
                videoview.setBackground(getResources().getDrawable(R.drawable.ic_music_headphones));
                pDialog.setTitle("Please wait Audio Streaming");
                asyncVideo(uri);
            }

        } else {

            videoview.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            Picasso.with(this).load(uri).into(imageView);
        }
    }

    public void asyncVideo(Uri uri) {
        // Set progressbar message
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();


        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    VideoPlayActivity.this);
            mediacontroller.setAnchorView(videoview);
            // Get the URL from String VideoURL

            videoview.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(uri);


        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            if (url.endsWith(".mp3")) {
                Toast.makeText(this, "Can't play audio", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Can't play video", Toast.LENGTH_SHORT).show();
            }
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoview.start();
            }
        });
    }

}

