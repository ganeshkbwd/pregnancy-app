package com.example.neha.pregnancyapp.utility;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by neha on 9/17/2016.
 */
public class Constants {

    public static final int SELECT_FILE = 11;
    public static final int REQUEST_CAMERA = 12;
    public static String encodedImage;
    public static Bitmap profileImage;
    public static int MONTHID;
    public static ProgressDialog progressDialog;
    public static int loggedin, user_id;
    public static Bitmap photo;
    public static List<String> month_string;
    public static ImageView imageView;


}
