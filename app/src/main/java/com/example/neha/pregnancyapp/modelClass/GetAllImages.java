package com.example.neha.pregnancyapp.modelClass;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neha on 9/15/2016.
 */
public class GetAllImages implements Serializable {
    String imagePath;
    int month;
    List<String> month_image_list;


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public List<String> getMonth_image_list() {
        return month_image_list;
    }

    public void setMonth_image_list(List<String> month_image_list) {
        this.month_image_list = month_image_list;
    }
}
