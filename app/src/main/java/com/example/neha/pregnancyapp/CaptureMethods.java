package com.example.neha.pregnancyapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.utility.Constants;
import com.example.neha.pregnancyapp.utility.ScalingUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 9/17/2016.
 */
public class CaptureMethods {


    public static void selectImage(final Activity activity) {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    activity.startActivityForResult(intent, Constants.REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    activity.startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            Constants.SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public static void onSelectFromGalleryResult(Intent data, Activity activity, ImageView ivProfile) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity.managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArrayImage = stream.toByteArray();
        Constants.encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);


        ivProfile.setImageBitmap(scaledBitmap);
        Constants.profileImage = scaledBitmap;


    }


    public static void onCaptureImageResult(Intent data, Activity activity, ImageView ivProfile) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        Constants.encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);


        ivProfile.setImageBitmap(scaledBitmap);
        Constants.profileImage = scaledBitmap;

    }

    public static void imageUpload(final Activity activity, final ImageView ivProfile) {
//        getStringImage(bitmap);

        Constants.progressDialog = new ProgressDialog(activity);
        Constants.progressDialog.setMessage("Uploading ....");
        Constants.progressDialog.show();

        final int month = Constants.MONTHID + 1;
        String uploadurl = "http://designer321.com/rajohn/pregnacycapp/api/save_monthly_progress.php";

        RequestQueue queue = Volley.newRequestQueue(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, uploadurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");

                            if (message_code == 1) {

//                                ivProfile.setImageBitmap(Constants.photo);
                                Constants.progressDialog.dismiss();
                            } else {
                                String message = jobj.getString("message");
                                Constants.progressDialog.dismiss();
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.progressDialog.dismiss();

                Toast.makeText(activity, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = Constants.encodedImage;
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", Constants.user_id + "");
                params.put("month", month + "");
                params.put("image_path", image);

                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);
    }

}
