package com.example.neha.pregnancyapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.neha.pregnancyapp.R;
import com.example.neha.pregnancyapp.modelClass.DashboardData;


/**
 * Created by neha on 9/12/2016.
 */
public class OneFragment extends Fragment {
    String testData;
    DashboardData dashboardData = new DashboardData();

    public OneFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        dashboardData = (DashboardData) this.getArguments().getSerializable("RESPONSE_DATA");
//        String dashboardDataStr = this.getArguments().getString("RESPONSE_DATA");


        View rootView = inflater.inflate(R.layout.fragment_one, container, false);
        TextView testDataF = (TextView) rootView.findViewById(R.id.testDataF);
        testDataF.setText(dashboardData.getDescription());
//        Log.e("Fragment", dashboardData.getDescription());

        return rootView;
    }

}
