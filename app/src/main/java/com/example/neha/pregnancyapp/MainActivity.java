package com.example.neha.pregnancyapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.neha.pregnancyapp.modelClass.GetAllImages;
import com.example.neha.pregnancyapp.mylib.CircularView;
import com.example.neha.pregnancyapp.mylib.Marker;
import com.example.neha.pregnancyapp.mylib.SimpleCircularViewAdapter;
import com.example.neha.pregnancyapp.utility.Constants;
import com.example.neha.pregnancyapp.utility.NumberPickerView;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private MySimpleCircularViewAdapter mAdapter;
    private CircularView circularView;
    Toolbar toolbar;
    String[] month_img = {
            "" + R.drawable.girl_default_pic,
            "" + R.drawable.get_ready_text,
            "" + R.drawable.missed_period_text,
            "" + R.drawable.month3,
            "" + R.drawable.month4,
            "" + R.drawable.month5,
            "" + R.drawable.month6,
            "" + R.drawable.month7,
            "" + R.drawable.month8,
            "" + R.drawable.month9,
            "" + R.drawable.mom_baby_default_pic,
    };
    Drawable drawable1;
    List<String> mont_string;
    int[] month_images = {R.drawable.girl_default_pic, R.drawable.get_ready_text, R.drawable.missed_period_text, R.drawable.month3, R.drawable.month4, R.drawable.month5, R.drawable.month6, R.drawable.month7, R.drawable.month8, R.drawable.month9, R.drawable.mom_baby_default_pic,};
    private GoogleApiClient client;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    LinearLayout settingDrawerLL;
    ProgressDialog progressDialog;
    public static final String MyPREFERENCES = "PregnancyAppPrefs";
    int loggedin, user_id, weeks;
    SharedPreferences sharedPreferences;
    GetAllImages getAllImages = new GetAllImages();
    Button weekBTN;
    SharedPreferences sharedpreferences;
    int WEEK = 0;
    long dateinMillis;
    private NumberPickerView numberPickerView;
    ArrayList<String> newMonthImage;
    ArrayList<GetAllImages> allImagesArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbarDrawer();
//        month_string = new ArrayList<String>();
//        month_string.add(String.valueOf(R.drawable.girl_default_pic));
//        month_string.add(String.valueOf(R.drawable.get_ready_text));
//        month_string.add(String.valueOf(R.drawable.missed_period_text));
//        month_string.add(String.valueOf(R.drawable.month3));
//        month_string.add(String.valueOf(R.drawable.month4));
//        month_string.add(String.valueOf(R.drawable.month5));
//        month_string.add(String.valueOf(R.drawable.month6));
//        month_string.add(String.valueOf(R.drawable.month7));
//        month_string.add(String.valueOf(R.drawable.month8));
//        month_string.add(String.valueOf(R.drawable.month9));
//        month_string.add(String.valueOf(R.drawable.mom_baby_default_pic));
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
////        String monthStr = sharedPreferences.getString("imgList",null);
//        Set<String> newset = new HashSet<String>();
//        Set<String> monthImagesList = sharedPreferences.getStringSet("imgList", newset);
//        newMonthImage = new ArrayList<String>(monthImagesList);
//        Log.e("NEWARRAY", newMonthImage.toString());

        Bundle bundle = getIntent().getExtras();

        allImagesArrayList = (ArrayList<GetAllImages>) bundle.getSerializable("response_images");

        Log.e("SERIALIZE", allImagesArrayList.toString());

        circularView = (CircularView) findViewById(R.id.circular_view);
        mAdapter = new MySimpleCircularViewAdapter();
        circularView.setAdapter(mAdapter);

        for (int i = 0; i < allImagesArrayList.size(); i++) {
            Constants.month_string.set(allImagesArrayList.get(i).getMonth(), allImagesArrayList.get(i).getImagePath());
//            allImagesArrayList.get(i).getMonth();
//            allImagesArrayList.get(i).getImagePath();
            Log.e("MONTHSSS ::", Constants.month_string.toString());
        }
        Log.e("MONTHSSS WHOLE ::", Constants.month_string.toString());
//        getallMonthImages(user_id);


        // Allow markers to continuously animate on their own when the highlight animation isn't running.
        // The flag can also be set in XML
//        circularView.setAnimateMarkerOnStillHighlight(true);
        // Combine the above line with the following so that the marker at it's position will animate at the start.
        // The highlighted Degree can also be defined in XML
//        circularView.setHighlightedDegree(CircularView.BOTTOM);

        circularView.setOnCircularViewObjectClickListener(new CircularView.OnClickListener() {
            @Override
            public void onClick(final CircularView view) {
//                Toast.makeText(MainActivity.this, "Clicked center", Toast.LENGTH_SHORT).show();

                // Start animation from the bottom of the circle, going clockwise.
//                final float start = CircularView.BOTTOM;
//                final float end = start + 360f + (float) (Math.random() * 720f);
//                // animate the highlighted degree value but also make sure it isn't so fast that it's skipping marker animations.
//                final long duration = (long) (Marker.ANIMATION_DURATION * 2L * end / (270L - mAdapter.getCount()));
//
//                circularView.animateHighlightedDegree(start, end, duration);
            }


            public void onMarkerClick(CircularView view, Marker marker, int position) {

                switch (position) {
                    case 0:

                        Toast.makeText(MainActivity.this, "You Clicked Girl Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:

                        Toast.makeText(MainActivity.this, "You Clicked Missed Period? Congo.. Start healthy to Stay Healthy", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(MainActivity.this, "You Clicked Get Ready for the Best Experience of your Life", Toast.LENGTH_SHORT).show();

                        break;
                    case 3:
                        Toast.makeText(MainActivity.this, "You Clicked Month3 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(MainActivity.this, "You Clicked Month4 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        Toast.makeText(MainActivity.this, "You Clicked Month5 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 6:
                        Toast.makeText(MainActivity.this, "You Clicked Month6 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 7:
                        Toast.makeText(MainActivity.this, "You Clicked Month7 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 8:
                        Toast.makeText(MainActivity.this, "You Clicked Month8 Pick", Toast.LENGTH_SHORT).show();
                        break;
                    case 9:
                        Toast.makeText(MainActivity.this, "You Clicked Month9 Pick", Toast.LENGTH_SHORT).show();

                        break;
                    case 10:
                        Toast.makeText(MainActivity.this, "You Clicked Girl & Baby Pick", Toast.LENGTH_SHORT).show();
                        break;
                    default:
//                        Toast.makeText(MainActivity.this, "You Clicked Girl Pick", Toast.LENGTH_SHORT).show();
                        break;
                }
                if (position != 0) {
                    Intent intent = new Intent(MainActivity.this, MonthProgressActivity.class);
                    intent.putExtra("MarkerId", position);
                    intent.putExtra("ComeFrom", "MainActivity");
                    startActivity(intent);
                } else {


                }


//                marker.setVisibility(marker.getVisibility() == View.INVISIBLE || marker.getVisibility() == View.GONE ? View.VISIBLE : View.INVISIBLE);
//                circularView.setTextSize(24 + position);
//                circularView.setTextColor(Color.BLACK);
            }
        });

//        circularView.setOnHighlightAnimationEndListener(new CircularView.OnHighlightAnimationEndListener() {
//            @Override
//            public void onHighlightAnimationEnd(CircularView view, Marker marker, int position) {
//                Toast.makeText(MainActivity.this, "Spin ends on " + marker.getId(), Toast.LENGTH_SHORT).show();
////                marker.setVisibility(marker.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
//                circularView.setTextColor(Color.BLUE);
//            }
//        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }


    public void setToolbarDrawer() {

        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);


        loggedin = sharedPreferences.getInt("login", 0);
        Log.e("loginid", loggedin + "");
        user_id = sharedPreferences.getInt("user_id", 0);
        weeks = sharedPreferences.getInt("week", 0);
        dateinMillis = sharedPreferences.getLong("date", 0);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        settingDrawerLL = (LinearLayout) findViewById(R.id.setting_drawer);
        Button dashboardTV = (Button) findViewById(R.id.dashboardTV);
        Button monthalyProgressTV = (Button) findViewById(R.id.monthalyProgressTV);
        Button aboutUsTV = (Button) findViewById(R.id.aboutUsTV);
        Button helpTV = (Button) findViewById(R.id.helpTV);
        Button logoutTV = (Button) findViewById(R.id.logoutBTN);
        weekBTN = (Button) findViewById(R.id.weekBTN);
        if (weeks != 0) {
            long diff = System.currentTimeMillis() - dateinMillis;
            long weekinmillis = 604800000;
            if (diff > weekinmillis) {
                int noOfWeek = (int) (diff / weekinmillis);
                weeks = weeks + noOfWeek;
                SharedPreferences sharedPreferences1 = getSharedPreferences(MyPREFERENCES, 0);
                SharedPreferences.Editor editor = sharedPreferences1.edit();
                editor.putInt("week", weeks);
                weekBTN.setText(weeks + " WEEK");
            } else {
                weekBTN.setText(weeks + " WEEK");
            }

        } else {
            weekBTN.setText("SET WEEK");
        }
        dashboardTV.setOnClickListener(this);
        monthalyProgressTV.setOnClickListener(this);
        aboutUsTV.setOnClickListener(this);
        helpTV.setOnClickListener(this);
        logoutTV.setOnClickListener(this);
        weekBTN.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dashboardTV:
                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "main");
                break;
            case R.id.monthalyProgressTV:

                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "main");
                break;
            case R.id.aboutUsTV:
                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "main");
                break;
            case R.id.helpTV:
                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "main");
                break;
            case R.id.logoutBTN:
                SplashScreenActivity.onDrawerOptionClick(this, v.getId(), "main");
                break;
            case R.id.weekBTN:
                setWeeks();
                break;
        }
    }

    public void setWeeks() {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.week_setting_dialog
                , null));
        numberPickerView = (NumberPickerView) settingsDialog.findViewById(R.id.details_quantity_npv);
        Button set = (Button) settingsDialog.findViewById(R.id.setBTN);
        Button cancel = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time =&gt; " + c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                System.out.println("Current time =&gt; " + formattedDate);
                WEEK = numberPickerView.getValue();

                SharedPreferences sharedPreferences1 = getSharedPreferences(MyPREFERENCES, 0);
                SharedPreferences.Editor editor = sharedPreferences1.edit();
                editor.putInt("week", WEEK);
                editor.putLong("date", System.currentTimeMillis());
                editor.commit();
                weekBTN.setText(WEEK + " WEEK");

                settingsDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();

    }


//    public void getallMonthImages(int pregnancy_user_id) {
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading ....");
//        progressDialog.show();
//
//        RequestQueue queue = Volley.newRequestQueue(this);
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/pregnacycapp/api/get_all_monthly_images.php?user_id=" + pregnancy_user_id,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            Log.e("RESPONSE", response);
//                            JSONObject jobj = new JSONObject(response);
//
//                            int message_code = jobj.getInt("message_code");
//                            if (message_code == 1) {
//
//                                JSONArray jsonArray = jobj.getJSONArray("response");
//                                for (int i = 0; i <= jsonArray.length(); i++) {
//                                    getAllImages.setImagePath(jsonArray.getJSONObject(i).getString("image_path"));
//                                    getAllImages.setMonth(jsonArray.getJSONObject(i).getInt("month"));
////                                    month_string.set(jsonArray.getJSONObject(i).getInt("month") + 2, jsonArray.getJSONObject(i).getString("image_path"));
//
//
//                                }
////                                Log.e("ARRAY", month_string.toString());
//                                progressDialog.dismiss();
//
//
////                                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
////                                Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
////                                startActivity(intent);
////                                finish();
//                            } else {
//                                String message = jobj.getString("message");
//                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
//                            }
//                            progressDialog.dismiss();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//
//                Toast.makeText(MainActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        queue.add(stringRequest);
//
//
//    }

    public class MySimpleCircularViewAdapter extends SimpleCircularViewAdapter {
        int count = 11;

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public void setupMarker(final int position, final Marker marker) {

//            if (getAllImages.getMonth() < 12) {
//                month_string.set(getAllImages.getMonth() + 2, getAllImages.getImagePath());
//
//            }
            for (int i = 0; i < Constants.month_string.size(); i++) {
                Log.e("SIZE", Constants.month_string.size() + "");

                if (i < Constants.month_string.size()) {
                    Log.e("CONSTANTSS :: ", Constants.month_string.get(i).toString());
                    if (Constants.month_string.get(i).toString().endsWith(".png")) {
//                        UrlToDrawable urlToDrawable = new UrlToDrawable(Constants.month_string.get(i).toString());
//                        urlToDrawable.execute();
//                        LoadImageFromWebOperations(Constants.month_string.get(i).toString());

//                        marker.setSrc(drawable1);
                        marker.setSrc(LoadImageFromWebOperations(Constants.month_string.get(i).toString()));
//                        try {
//                            URL url = new URL(Constants.month_string.get(i));
//                            Bitmap bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());
//                            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
//                            marker.setSrc(drawable);
//                        } catch (MalformedURLException e) {
//                            e.printStackTrace();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    } else {

                        marker.setSrc(getResources().getDrawable(Integer.parseInt(Constants.month_string.get(i))));
                    }

                }


//            }
//            if (month_img.length > position)
//                marker.setSrc(getResources().getDrawable(Integer.parseInt(month_img[position])));
////            }
                marker.setRadius(50f);

//            marker.setFitToCircle(true);
//            marker.setRadius(10 + 2 * position);
            }
        }

//        public class UrlToDrawable extends AsyncTask<Void, String, Drawable> {
//
//            String url;
//
//            public UrlToDrawable(String s) {
//                url = s;
//            }
//
//            @Override
//            protected Drawable doInBackground(Void... params) {
//
//                InputStream is = null;
//                Drawable d = null;
//                try {
//                    is = new URL(url).openStream();
//                    d = Drawable.createFromStream(is, "marker");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//
//                return d;
//            }
//
//            @Override
//            protected void onPostExecute(Drawable drawable) {
//
//                drawable1 = drawable;
//            }
//        }

        public Drawable LoadImageFromWebOperations(String url) {
            try {
//                if (android.os.Build.VERSION.SDK_INT > 9) {
//                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//                    StrictMode.setThreadPolicy(policy);
                InputStream is = new URL(url).openStream();
                Drawable d = Drawable.createFromStream(is, "marker");
                return d;
//                } else {
//                    return null;
//                }

            } catch (Exception e) {
                return null;
            }
        }

        public Bitmap getImageBitmapFromUrl(URL url) {
            Bitmap bm = null;
            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if (conn.getResponseCode() != 200) {
                    return bm;
                }
                conn.connect();
                InputStream is = conn.getInputStream();

                BufferedInputStream bis = new BufferedInputStream(is);
                try {
                    bm = BitmapFactory.decodeStream(bis);
                } catch (OutOfMemoryError ex) {
                    bm = null;
                }
                bis.close();
                is.close();
            } catch (Exception e) {
            }

            return bm;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                SplashScreenActivity.logout_fun(MainActivity.this);
                break;
        }
//        Toast.makeText(MainActivity.this, "Object count " + mAdapter.getCount(), Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MainActivity.this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }
}

