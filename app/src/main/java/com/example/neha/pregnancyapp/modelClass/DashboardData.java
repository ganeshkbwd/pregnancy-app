package com.example.neha.pregnancyapp.modelClass;

import java.io.Serializable;

/**
 * Created by neha on 9/12/2016.
 */
public class DashboardData implements Serializable {
    String title;
    String description;
    String menu_name;
    String preg_media1;
    String preg_media2;
    String preg_media3;
    String preg_media4;
    String time;
    String price;
    String name;
    String hospital_name;
    String hospital_address;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getPreg_media1() {
        return preg_media1;
    }

    public void setPreg_media1(String preg_media1) {
        this.preg_media1 = preg_media1;
    }

    public String getPreg_media2() {
        return preg_media2;
    }

    public void setPreg_media2(String preg_media2) {
        this.preg_media2 = preg_media2;
    }

    public String getPreg_media3() {
        return preg_media3;
    }

    public void setPreg_media3(String preg_media3) {
        this.preg_media3 = preg_media3;
    }

    public String getPreg_media4() {
        return preg_media4;
    }

    public void setPreg_media4(String preg_media4) {
        this.preg_media4 = preg_media4;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getHospital_address() {
        return hospital_address;
    }

    public void setHospital_address(String hospital_address) {
        this.hospital_address = hospital_address;
    }


}
