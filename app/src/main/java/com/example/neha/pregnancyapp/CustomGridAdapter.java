package com.example.neha.pregnancyapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by neha on 9/7/2016.
 */
public class CustomGridAdapter extends BaseAdapter{
    Context context;
    int[] month_images;

    public CustomGridAdapter(Activity activity, int[] month_images) {
        this.context = activity;
        this.month_images = month_images;
    }

    @Override
    public int getCount() {
        return month_images.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class Holder
    {
//        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.grid_item_layout, null);

        holder.img=(ImageView) rowView.findViewById(R.id.imageViewGrid);


        holder.img.setImageResource(month_images[position]);

//        rowView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked "+month_images[position], Toast.LENGTH_LONG).show();
//            }
//        });

        return rowView;
    }
}
