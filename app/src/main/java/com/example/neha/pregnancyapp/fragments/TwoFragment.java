package com.example.neha.pregnancyapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.neha.pregnancyapp.R;
import com.example.neha.pregnancyapp.VideoPlayActivity;
import com.example.neha.pregnancyapp.modelClass.DashboardData;


/**
 * Created by neha on 9/12/2016.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {
    DashboardData dashboardData;
    String urlMedia1, urlMedia2, urlMedia3, urlMedia4;
    TextView testDataM1, testDataM2, testDataM3, testDataM4;
    private static ProgressDialog progressDialog;
    private SeekBar seekBarProgress;
    Button buttonPlayPause;
    private MediaPlayer mediaPlayer;
    private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class
    boolean isBack = false;
    private final Handler handler = new Handler();
    String finalUrl;
    ImageView cardView, cardView1, cardView2, cardView3;
    RelativeLayout media1LL, media2LL, media3LL, media4LL;
//    LinearLayout testDataM1LL;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        dashboardData = (DashboardData) this.getArguments().getSerializable("RESPONSE_DATA");
        View rootView = inflater.inflate(R.layout.media_layout, container, false);
        viewSet(rootView);
//        seekBarSet(rootView);

        return rootView;
    }

//    private void seekBarSet(View rootView) {


//        buttonPlayPause = (Button) rootView.findViewById(R.id.playpause);
//        buttonPlayPause.setOnClickListener(this);
//
//        seekBarProgress = (SeekBar) rootView.findViewById(R.id.seekBar);
//        seekBarProgress.setMax(99); // It means 100% .0-99
//        seekBarProgress.setOnTouchListener(this);
//
//        mediaPlayer = new MediaPlayer();
//        mediaPlayer.setOnBufferingUpdateListener(this);
//        mediaPlayer.setOnCompletionListener(this);
//}


    /**
     * Method which updates the SeekBar primary progress by current song playing position
     */
    private void primarySeekBarProgressUpdater() {
        seekBarProgress.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.media1LL:
                intent = new Intent(getActivity(), VideoPlayActivity.class);
                intent.putExtra("URL", urlMedia1);
                startActivity(intent);
                break;
            case R.id.media2LL:
                intent = new Intent(getActivity(), VideoPlayActivity.class);
                intent.putExtra("URL", urlMedia2);
                startActivity(intent);
                break;
            case R.id.media3LL:
                intent = new Intent(getActivity(), VideoPlayActivity.class);
                intent.putExtra("URL", urlMedia3);
                startActivity(intent);
                break;
            case R.id.media4LL:
                intent = new Intent(getActivity(), VideoPlayActivity.class);
                intent.putExtra("URL", urlMedia4);
                startActivity(intent);
                break;
        }

    }

//    public void playpausecode(String url) {
//        try {
//            mediaPlayer.setDataSource(url);
//            mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
//
//        if (!mediaPlayer.isPlaying()) {
//            mediaPlayer.start();
//            buttonPlayPause.setText("PAUSE");
//        } else {
//            mediaPlayer.pause();
//            buttonPlayPause.setText("PLAY");
//        }
//
//        primarySeekBarProgressUpdater();
//    }

//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        if (v.getId() == R.id.seekBar) {
//            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
//            if (mediaPlayer.isPlaying()) {
//                SeekBar sb = (SeekBar) v;
//                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
//                mediaPlayer.seekTo(playPositionInMillisecconds);
//            }
//        }
//        return false;
//    }


//    @Override
//    public void onCompletion(MediaPlayer mp) {
//        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
//        buttonPlayPause.setText("PLAY");
//    }
//
//    @Override
//    public void onBufferingUpdate(MediaPlayer mp, int percent) {
//        /** Method which updates the SeekBar secondary progress by current song loading from URL position*/
//        seekBarProgress.setSecondaryProgress(percent);
//    }


    public void viewSet(View rootView) {
        cardView = (ImageView) rootView.findViewById(R.id.card_view);
        cardView1 = (ImageView) rootView.findViewById(R.id.card_view1);
        cardView2 = (ImageView) rootView.findViewById(R.id.card_view2);
        cardView3 = (ImageView) rootView.findViewById(R.id.card_view3);

        media1LL = (RelativeLayout) rootView.findViewById(R.id.media1LL);
        media2LL = (RelativeLayout) rootView.findViewById(R.id.media2LL);
        media3LL = (RelativeLayout) rootView.findViewById(R.id.media3LL);
        media4LL = (RelativeLayout) rootView.findViewById(R.id.media4LL);

        testDataM1 = (TextView) rootView.findViewById(R.id.testDataM1);
        testDataM2 = (TextView) rootView.findViewById(R.id.testDataM2);
        testDataM3 = (TextView) rootView.findViewById(R.id.testDataM3);
        testDataM4 = (TextView) rootView.findViewById(R.id.testDataM4);
//        testDataM1LL = (LinearLayout) rootView.findViewById(R.id.testDataM1LL);

//        Log.e("TWO FRAG",dashboardData.getPreg_media2());
        if (dashboardData.getPreg_media1().toString().equalsIgnoreCase("") && dashboardData.getPreg_media2().toString().equalsIgnoreCase("-")
                && dashboardData.getPreg_media3().toString().equalsIgnoreCase("-") && dashboardData.getPreg_media4().toString().equalsIgnoreCase("-")) {
            cardView.setVisibility(View.VISIBLE);
            cardView1.setVisibility(View.INVISIBLE);
            cardView2.setVisibility(View.INVISIBLE);
            cardView3.setVisibility(View.INVISIBLE);
            testDataM1.setText("No Media content available");
            testDataM1.setClickable(false);

        } else {
            if (!dashboardData.getPreg_media1().equalsIgnoreCase("")) {
                cardView.setVisibility(View.VISIBLE);
                urlMedia1 = dashboardData.getPreg_media1().toString();
                String[] newurl = urlMedia1.split("/");
                testDataM1.setText(newurl[newurl.length - 1]);
                if (!urlMedia1.endsWith(".mp3") || !urlMedia1.endsWith(".mp4") && !urlMedia1.endsWith(".mov"))
                    Glide.with(getContext()).load(Uri.parse(urlMedia1)).into(cardView);

//                testDataM1LL.setBackground(getResources().getDrawable(R.drawable.ic_music_headphones));

            } else {
                media1LL.setVisibility(View.INVISIBLE);
            }

            if (!dashboardData.getPreg_media2().equalsIgnoreCase("-")) {
                cardView1.setVisibility(View.VISIBLE);
                urlMedia2 = dashboardData.getPreg_media2().toString();
                String[] newurl = urlMedia2.split("/");
                testDataM2.setText(newurl[newurl.length - 1]);
                if (!urlMedia2.endsWith(".mp3") || !urlMedia2.endsWith(".mp4") && !urlMedia2.endsWith(".mov"))
                    Glide.with(getContext()).load(Uri.parse(urlMedia2)).into(cardView1);

            } else {
                media2LL.setVisibility(View.INVISIBLE);
            }

            if (!dashboardData.getPreg_media3().equalsIgnoreCase("-")) {
                cardView2.setVisibility(View.VISIBLE);
                urlMedia3 = dashboardData.getPreg_media3().toString();
                String[] newurl = urlMedia3.split("/");
                testDataM3.setText(newurl[newurl.length - 1]);
                if (!urlMedia3.endsWith(".mp3") && !urlMedia3.endsWith(".mp4") && !urlMedia3.endsWith(".mov"))
                    Glide.with(getContext()).load(Uri.parse(urlMedia3)).into(cardView2);

            } else {
                media3LL.setVisibility(View.INVISIBLE);
            }

            if (!dashboardData.getPreg_media4().equalsIgnoreCase("-")) {
                cardView3.setVisibility(View.VISIBLE);
                urlMedia4 = dashboardData.getPreg_media4().toString();
                String[] newurl = urlMedia4.split("/");

                testDataM4.setText(newurl[newurl.length - 1]);
                if (!urlMedia4.endsWith(".mp3") && !urlMedia4.endsWith(".mp4") && !urlMedia4.endsWith(".mov"))
                    Glide.with(getContext()).load(Uri.parse(urlMedia4)).into(cardView3);

            } else {
                media4LL.setVisibility(View.INVISIBLE);
            }

            media1LL.setOnClickListener(this);
            media2LL.setOnClickListener(this);
            media3LL.setOnClickListener(this);
            media4LL.setOnClickListener(this);
        }

    }


}
