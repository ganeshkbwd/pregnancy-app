package com.example.neha.pregnancyapp;

import android.animation.Animator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.pregnancyapp.utility.Constants;
import com.example.neha.pregnancyapp.utility.ScalingUtilities;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;


public class MonthProgressActivity extends AppCompatActivity {
    private static final int SELECT_FILE = 11;
    private static final int REQUEST_CAMERA = 12;
    Toolbar toolbar;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;
    ImageView ivProfile;
    int MONTHID;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    //    int loggedin, user_id;
    public static final String MyPREFERENCES = "PregnancyAppPrefs";


    private Button btnCapturePicture, btnRecordVideo;
    private String encodedImage;
    private Bitmap profileImage;
    private GoogleApiClient client;
    Bitmap photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_progress);
        setToolbarDrawer();
        ivProfile = (ImageView) findViewById(R.id.imageView);

        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        Constants.loggedin = sharedPreferences.getInt("login", 0);
        Constants.user_id = sharedPreferences.getInt("user_id", 0);
        optionClicked();

        getMonthImage();
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CaptureMethods.selectImage(MonthProgressActivity.this);
                selectImage();
            }
        });

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MonthProgressActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);


        ivProfile.setImageBitmap(scaledBitmap);
        photo = scaledBitmap;
        imageUpload();

    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);


        ivProfile.setImageBitmap(scaledBitmap);
        photo = scaledBitmap;
        imageUpload();

    }


    public void imageUpload() {
//        getStringImage(bitmap);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading ....");
        progressDialog.show();

        final int month = MONTHID + 1;
        String uploadurl = "http://designer321.com/rajohn/pregnacycapp/api/save_monthly_progress.php";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, uploadurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");

                            if (message_code == 1) {

                                ivProfile.setImageBitmap(photo);
                                progressDialog.dismiss();
                            } else {
                                String message = jobj.getString("message");
                                progressDialog.dismiss();
                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(MonthProgressActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = encodedImage;
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(Constants.user_id));
                params.put("month", month + "");
                params.put("image_path", image);

                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }

    public void getMonthImage() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.show();

        int month = MONTHID + 1;
        String uploadurl = "http://designer321.com/rajohn/pregnacycapp/api/get_selected_month_images.php?user_id=" + Constants.user_id + "&month=" + month;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, uploadurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
                            if (message_code == 1) {
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                String image_path = jsonArray.getJSONObject(0).getString("image_path");

                                Picasso.with(MonthProgressActivity.this).load(image_path).into(ivProfile);
                                progressDialog.dismiss();
                            } else {
                                String message = jobj.getString("message");
                                progressDialog.dismiss();
                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(MonthProgressActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });


        queue.add(stringRequest);
    }

    public void optionClicked() {
        Bundle bundle = getIntent().getExtras();
        String comeFrom = bundle.getString("ComeFrom");
        if (comeFrom.equalsIgnoreCase("MainActivity")) {

            getSupportActionBar().setTitle("Monthly Progress");
            MONTHID = bundle.getInt("MarkerId");
            switch (MONTHID) {
                case 0:
                    getSupportActionBar().setTitle("Girl");
                    break;
                case 1:
                    getSupportActionBar().setTitle("Congratulations");
                    break;
                case 2:
                    getSupportActionBar().setTitle("Best Experience of Life");

                    break;
                case 3:
                    getSupportActionBar().setTitle("Month 3");
                    break;
                case 4:
                    getSupportActionBar().setTitle("Month 4");
                    break;
                case 5:
                    getSupportActionBar().setTitle("Month 5");
                    break;
                case 6:
                    getSupportActionBar().setTitle("Month 6");
                    break;
                case 7:
                    getSupportActionBar().setTitle("Month 7");
                    break;
                case 8:
                    getSupportActionBar().setTitle("Month 8");
                    break;
                case 9:
                    getSupportActionBar().setTitle("Month 9");
                    break;
                case 10:

                    getSupportActionBar().setTitle("Girl & Baby");
                    break;

                default:
                    break;

            }

        }

    }

    public void setToolbarDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView title = (TextView) toolbar.findViewById(R.id.titleToolbar);
        title.setVisibility(View.GONE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                Intent selectedImageResultIntent = data;
//                CaptureMethods.onSelectFromGalleryResult(data, MonthProgressActivity.this, ivProfile);
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                //checkPermissions();
//                CaptureMethods.onCaptureImageResult(data, MonthProgressActivity.this, ivProfile);
                onCaptureImageResult(data);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MonthProgressActivity.this, MonthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
